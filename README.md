# README #

SUMMARY

2016 iOS Source Code example app.

a) Latest XCode 7.2 SDK 9.x.

b) Full Storyboard support. 

c) Full Core Data Framework with local store SQLite (CRUD - Update, Delete, Add). 

d) Core Data Fetch Controller and Key-Value Observation Demonstrated. 

e) Core Data integration with UITableView. 

f) Auto Layout, Base Class UI .

g) Custom Table View Cell Views. 

h) Table View Cells sub-view touch Block processing.


* Version - 1.0(1)



### How do I get set up? ###

* Summary of set up - Users can clone and or download project source code and be able to run debug versions. 

* Configuration - N/A
* Dependencies - Compile on XCode 7.2.x iOS SDK 9.x  March 2016

* Database configuration - SQLite store on Devices via the Core Data Framework.  No Network sync or configuration. 

None

* How to run tests - N/A
* Deployment instructions - This is an example source code project to be used to instruct and review some of the latest iOS SDK 9.x technologies.  This code as a project is not to be deployed by others.  It is deployed on the Apple App Store (March 15, 2016 In Review) by this repos owner(as a whole project). Open source code that can be replicated in sub-parts for anyones project.  The name is owned by the repo owner.

### Contribution guidelines ###

* Writing tests - N/A
* Code review - N/A
* Other guidelines - N/A

### Who do I talk to? ###

* Repo owner or admin 

Matthew Ferguson
matthew.ferguson@zoho.com

* Other community or team contact - N/A