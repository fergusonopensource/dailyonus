//
//  SingleOnusCell.h
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import <UIKit/UIKit.h>

typedef void (^SingleOnusCellDidTapButtonBlock)();

@interface SingleOnusCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel * taskLabel;
@property (weak, nonatomic) IBOutlet UIButton * completedOnusButton;

@property (copy, nonatomic) SingleOnusCellDidTapButtonBlock didTapButtonBlock;

@end
