//
//  SingleOnusCell.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import "SingleOnusCell.h"


@implementation SingleOnusCell


- (void)awakeFromNib {
    
    // Initialization code
    [super awakeFromNib];
    
    // Setup View
    [self setupView];
}




- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



#pragma mark -
#pragma mark View Methods
- (void)setupView {
    
    UIImage *imageNormal = [UIImage imageNamed:@"buttonNormalComplete"];
    UIImage *imageSelected = [UIImage imageNamed:@"buttonSelectedComplete"];
    
    [self.completedOnusButton setImage:imageNormal forState:UIControlStateNormal];
    [self.completedOnusButton setImage:imageNormal forState:UIControlStateDisabled];
    [self.completedOnusButton setImage:imageSelected forState:UIControlStateSelected];
    [self.completedOnusButton setImage:imageSelected forState:UIControlStateHighlighted];
    [self.completedOnusButton addTarget:self action:@selector(didTapButton:) forControlEvents:UIControlEventTouchUpInside];

}



#pragma mark -
#pragma mark Actions
- (void)didTapButton:(UIButton *)button {
    
    if (self.didTapButtonBlock) {
        
        self.didTapButtonBlock();
    
    }
    
}


@end
