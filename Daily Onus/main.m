//
//  main.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/16/16.
//  Copyright © 2016 Matthew Ferguson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
