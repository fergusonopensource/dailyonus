//
//  UpdateSingleOnusViewController.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import "UpdateSingleOnusViewController.h"

@interface UpdateSingleOnusViewController ()

@end

@implementation UpdateSingleOnusViewController



- (void)viewDidLoad {
    
    [super viewDidLoad];
    if (self.record) {
        [self.textField setText:[self.record valueForKey:@"task"]];
    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark Actions
- (IBAction)cancel:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}



- (IBAction)save:(id)sender {
    

    // Helpers
    NSString *tempTask = self.textField.text;
    
    if (tempTask && tempTask.length) {

        // Populate Record
        [self.record setValue:tempTask forKey:@"task"];
        
        // Save Record
        NSError *error = nil;
        
        if ([self.managedObjectContext save:&error]) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            
            if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your to-do item could not be saved." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                       handler:nil];
            [alertController addAction:ok];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    } else {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your to-do needs a name." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
