//
//  RootViewController.h
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SingleOnusCell.h"
#import "AddSingleOnusViewController.h"
#import "UpdateSingleOnusViewController.h"

@interface RootViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSIndexPath *selection;

@end

