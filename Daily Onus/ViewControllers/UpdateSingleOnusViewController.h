//
//  UpdateSingleOnusViewController.h
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface UpdateSingleOnusViewController : UIViewController


@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSManagedObject *record;


@end
