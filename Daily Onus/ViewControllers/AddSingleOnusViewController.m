//
//  AddSingleOnusViewController.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import "AddSingleOnusViewController.h"

@interface AddSingleOnusViewController()

@end

@implementation AddSingleOnusViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -
#pragma mark Actions

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)save:(id)sender {
    
    
    // Helpers
    NSString *tempTask = self.textField.text;
    
    if (tempTask && tempTask.length) {
        // Create Entity
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"DailyOnusItem" inManagedObjectContext:self.managedObjectContext];
        
        // Initialize Record
        NSManagedObject *record = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:self.managedObjectContext];
    
        // Populate Record
        [record setValue:tempTask forKey:@"task"];
        [record setValue:[NSDate date] forKey:@"creationDate"];
        
        // Save Record
        NSError *error = nil;
        
        if ([self.managedObjectContext save:&error]) {
            
            // Dismiss View Controller
            [self dismissViewControllerAnimated:YES completion:nil];
            
        } else {
            
            if (error) {
                NSLog(@"Unable to save record.");
                NSLog(@"%@, %@", error, error.localizedDescription);
            }
    
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your to-do could not be saved to the device store." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alertController addAction:ok];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        
    } else {

        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Your to-do could not be saved.  Empty entry" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok =
            [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];

        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
