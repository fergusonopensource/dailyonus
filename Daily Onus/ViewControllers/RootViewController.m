//
//  RootViewController.m
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//


#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController



- (void)viewDidLoad {

    
    [super viewDidLoad];
    
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"DailyOnusItem"];
    
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]]];
    
    // Initialize Fetched Results Controller
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:(id)self];

    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




#pragma mark -
#pragma mark Table View UITableViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
    return [[self.fetchedResultsController sections] count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    return [sectionInfo numberOfObjects];

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SingleOnusCell *cell = (SingleOnusCell *)[tableView dequeueReusableCellWithIdentifier:@"SingleOnusCell" forIndexPath:indexPath];
    
    // Configure Table View Cell
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}




- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}




- (void)tableView:(UITableView *)tableView
    commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
    forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if (record) {
            [self.fetchedResultsController.managedObjectContext deleteObject:record];
        }
        
    }

}






#pragma mark -
#pragma mark Table View Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Store Selection
    [self setSelection:indexPath];
    
    [self performSegueWithIdentifier:@"updateToDoViewController" sender:self];

}




- (void)configureCell:(SingleOnusCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    
    NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Update Cell
    [cell.taskLabel setText:[record valueForKey:@"task"]];
    [cell.completedOnusButton setSelected:[[record valueForKey:@"completedOnus"] boolValue]];
    
    
    [cell setDidTapButtonBlock:^{
    
        BOOL isCompletedOnus = [[record valueForKey:@"completedOnus"] boolValue];
        // Update Record
        [record setValue:@(!isCompletedOnus) forKey:@"completedOnus"];
        
    }];
    
}




- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    
    switch (type) {
            
        case NSFetchedResultsChangeInsert: {
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
        case NSFetchedResultsChangeDelete: {
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
            
        case NSFetchedResultsChangeUpdate: {
            
            [self configureCell:(SingleOnusCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            
            break;
        }
            
        case NSFetchedResultsChangeMove: {
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            break;
        }
        
    }
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"addToDoViewController"]) {
    
        // Obtain Reference to View Controller
        UINavigationController *nc = (UINavigationController *)[segue destinationViewController];
        AddSingleOnusViewController *vc = (AddSingleOnusViewController *)[nc topViewController];
        
        // Configure View Controller
        [vc setManagedObjectContext:self.managedObjectContext];
        
    }
    else if ([segue.identifier isEqualToString:@"updateToDoViewController"]) {
    
        // Obtain Reference to View Controller
        UpdateSingleOnusViewController *vc = (UpdateSingleOnusViewController *)[segue destinationViewController];
    
        // Configure View Controller //managedObjectContext
        [vc setManagedObjectContext:self.managedObjectContext];
    
        if (self.selection) {
            // Fetch Record
            NSManagedObject *record = [self.fetchedResultsController objectAtIndexPath:self.selection];
            if (record) {
                [vc setRecord:record];
            }
            // Reset Selection
            [self setSelection:nil];
        }
    }
    
}

@end