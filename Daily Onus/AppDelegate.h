//
//  AppDelegate.h
//  Daily Onus
//
//  Created by Matthew Ferguson on 3/15/16.
//  matthew.ferguson@zoho.com
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "RootViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;


//Core Data properties are marked as readonly, which means that the instances
//  cannot be modified by objects other than the application delegate itself.
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;



- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;



@end

